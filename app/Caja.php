<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caja extends Model
{
    use SoftDeletes;
    protected $table = 'cajas';
    protected $fillable = [
        'caja_dia',
        'fecha_hora_apertura',
        'fecha_hora_cierre',
        'importe_apertura',
        'importe_cierre',
        'user_id',
        'total_movimientos_turno',
        'concepto_cierre'
    ];

    protected $dates =[
        'deleted_at',
        'caja_dia',
        'fecha_hora_apertura',
        'fecha_hora_cierre'
    ];

    protected $casts = [
        'caja_dia' => 'datetime',
        'fecha_hora_apertura' => 'datetime',
        'fecha_hora_cierre' => 'datetime'
    ];

    public function usuario(){
        return $this->belongsTo('App\User');
    }

    public function detalles(){
        return $this->hasMany('App\CajaDetalle');
    }
}
