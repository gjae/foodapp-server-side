<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CajaDetalle extends Model
{
    use SoftDeletes;
    protected $table = 'caja_detalles';
    protected $fillable = [
        'total',
        'factura_id',
        'caja_id',
        'proceso'
    ];

    
    public function caja(){
        return $this->belongsTo('App\Caja');
    }

    public function factura(){
        return $this->belongsTo('App\Factura');
    }
}
