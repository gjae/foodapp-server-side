<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoriaImagen extends Model
{
    use SoftDeletes;
    protected $table = 'categoria_imagens';
    protected $fillable = [
        'imagen_id',
        'comida_tipo_id'
    ];

    public function categorias(){
        return $this->belongsTo('App\ComidaTipo', 'comida_tipo_id');
    }

    public function imagen(){
        return $this->belongsTo('App\Imagen');
    }
}
