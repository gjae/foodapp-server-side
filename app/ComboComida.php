<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class ComboComida extends Model
{
    use SoftDeletes;
    protected $table = 'combo_comidas';
    protected $fillable = [
        'nombre',
        'descripcion',
        'costo',
        'disponibilidad',
        'valido_desde',
        'valido_hasta',
        'cantidad_disponible',
        'codigo'
    ];

    protected $dates = [
        'deleted_at', 
        'valido_desde',
        'valido_hasta'
    ];
    protected $casts = [
        'valido_desde' => 'datetime',
        'valido_hasta' => 'datetime'
    ];

    public function productos(){
        return $this->hasMany('App\ComboDetalle', 'combo_comidas_id', 'id');
    }

    public function facturas(){
        return $this->hasMany('App\DetalleFactura');
    }

    public function setValidoDesdeAttribute($old){
        $this->attributes['valido_desde'] = Carbon::parse($old)->format('Y-m-d H:i:s');
    }

    public function imagenes(){
        return $this->hasMany('App\ComboImagen', 'combo_comida_id');
    }


    public function label(){
        switch( $this->disponibilidad ){
            case 'DISPONIBLE':
                return 'label-success';
            break;
            case 'AGOTADO':
                return 'label-danger';
            break;
            default:
                return 'label-danger';
            break;

        } 
    }
}
