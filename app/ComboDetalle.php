<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComboDetalle extends Model
{
    use SoftDeletes;
    protected $table = 'combo_detalles';
    protected $fillable = [
        'comida_id',
        'cantidad_comida',
        'combo_comidas_id',

    ];

    public function comida(){
        return $this->belongsTo('App\Comida');
    }

    public function combo(){
        return $this->belongsTo('App\ComboComdida', 'combo_comida_id', 'id');
    }
}
