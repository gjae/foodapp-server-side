<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComboImagen extends Model
{
    use SoftDeletes;
    protected $table = 'combo_imagens';
    protected $fillable = [
        'combo_comida_id',
        'imagen_id'
    ];

    public function imagen(){
        return $this->belongsTo('App\Imagen');
    }

    public function combo(){
        return $this->belongsTo('App\ComboComida');
    }
}
