<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comida extends Model
{
    use SoftDeletes;
    protected $table = 'comidas';
    protected $fillable = [
        'nombre',
        'descripcion',
        'costo',
        'disponible',
        'user_id',
        'comida_tipo_id',
        'add_at',
        'codigo',
        'unidad_medida_id'
    ];

    protected $dates = [ 'add_at', 'deleted_at'];


    public function medida(){
        return $this->belongsTo('App\UnidadMedida', 'unidad_medida_id');
    }

    public function combos(){
        return $this->hasMany('App\ComboDetealle', 'comida_id', 'id');
    }

    public function tipo(){
        return $this->belongsTo('App\ComidaTipo', 'comida_tipo_id');
    }

    public function facturas(){
        return $this->hasMany('App\DetalleFactura');
    }

    public function imagenes(){
        return $this->hasMany('App\ImagenComida');
    }

    public function label(){
        switch( $this->disponible ){
            case 'SI':
                return 'label-success';
            break;
            case 'NO':
                return 'label-warning';
            break;
            case 'ACOGATADO':
                return 'label-danger';
            break;
        } 
    }
}
