<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComidaTipo extends Model
{
    use SoftDeletes;
    protected $table = 'comida_tipos';
    protected $fillable = [
        'tipo',
        'descripcion'
    ];

    public function comidas(){
        return $this->hasMany('App\Comida', 'comida_tipo_id', 'id');
    }

    public function imagenes(){
        return $this->hasMany('App\CategoriaImagen', 'comida_tipo_id', 'id');
    }
}
