<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleFactura extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'comida_id',
        'combo_comida_id',
        'cantidad',
        'precio_unitario',
        'precio_total',
        'total_impuesto',
        'factura_id',

    ];

    public function factura(){
        return $this->belongsTo('App\Factura');
    }

    public function comida(){
        return $this->belongsTo('App\Comida');
    }
    
    public function combos(){
        return $this->belongsTo('App\ComboComida', 'combo_comida_id', 'id');
    }

}
