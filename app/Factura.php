<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Factura extends Model
{
    use SoftDeletes;
    protected $table = 'facturas';
    protected $fillable = [
        'emision',
        'impuesto',
        'subtotal',
        'total',
        'total_pagado',
        'total_cambio',
        'medio_pago',
        'numero_medio',
        'numero_factura',
        'estado_factura',
        'fecha_anulacion',
        'user_titular_id',
        'user_registrador_id',
    ];

    protected $dates = ['deleted_at', 'emision', 'fecha_anulacion'];
    protected $casts = [
        'emision' => 'datetime',
        'fecha_anulacion' => 'datetime'
    ];

    public function detalles(){
        return $this->hasMany('App\DetalleFactura');
    }

    public function titular(){
        return $this->belongsTo('App\User','user_titular_id', 'id');
    }

    public function registrador(){
        return $this->belongsTo('App\User', 'user_registrador_id', 'id');
    }
}
