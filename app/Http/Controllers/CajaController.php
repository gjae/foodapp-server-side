<?php

namespace App\Http\Controllers;

use App\Caja;
use App\Factura;
use Carbon\Carbon;
use App\CajaDetalle;
use DB;
use PDF;
use Illuminate\Http\Request;

class CajaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $caja = Caja::whereDate('caja_dia', '=', Carbon::now()->format('Y-m-d'))->whereNull('fecha_hora_cierre')->first();

        if( is_null($caja) )
            return view('dashboard.caja.apertura', [
                'user' => auth()->user()
            ]);

        
        $diario = Factura::where('emision', '>=', $caja->caja_dia)->get() ;

        return view( 'dashboard.caja.movimientos', [
            'user' => auth()->user(),
            'facturas' => $diario,
            'caja' => $caja
        ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fecha = Carbon::now();

        $caja = Caja::whereDate('caja_dia', '=', $fecha->format('Y-m-d'))
        ->whereNull('fecha_hora_cierre')->get();

        if( $caja->isEmpty() )
            $caja = Caja::create([
                'caja_dia' => $fecha,
                'user_id' => auth()->user()->id,
                'fecha_hora_apertura' => $fecha,
                'importe_apertura' => $request->importe_apertura
            ]);

        return redirect()->to( url('dashboard/caja') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Caja  $caja
     * @return \Illuminate\Http\Response
     */
    public function show(Caja $caja)
    {
        $facturas = Factura::whereDate( 'emision', '=', $caja->caja_dia->format('Y-m-d') )->get();
        $total = 0.00;
        $movimientos = 0;

        DB::beginTransaction();

        try {
            foreach( $facturas as $factura ){
                CajaDetalle::create([
                    'caja_id' => $caja->id,
                    'factura_id' => $factura->id,
                    'total' => $factura->total,
                    'proceso' => 'CIERRE'
                ]);

                $total += $factura->total;
                $movimientos++;
            }

            $caja->fecha_hora_cierre = Carbon::now();
            $caja->importe_cierre = $total;
            $caja->save();
            DB::commit();
        }catch( \Exception $e){
            DB::rollback();
        }

        return redirect()->to( url( 'dashboard/caja' ) );
    }

    public function reporteDia($dia_id){
        $dia = Caja::find($dia_id);
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML(
            \View::make('dashboard.caja.reportes.cierre_dia', [
                'dia' => $dia
            ])->render()
        );
        
        return $pdf->stream('invoice', ['attachment' => 0]);
    }

    public function resumen(){
        $caja = Caja::selectRaw("
            sum(importe_apertura) as importe_apertura,
            sum(importe_cierre) as importe_cierre,
            date(fecha_hora_apertura) as fecha_hora_apertura,
            date(fecha_hora_cierre) as fecha_hora_cierre
        ")
        ->groupBy('fecha_hora_apertura')
        ->groupBy('fecha_hora_cierre')
        ->get();

        return view('dashboard.caja.resumen', [
            'user' => auth()->user(),
            'dias_caja' => Caja::all()
        ]);
    }

    public function tipoDeReporte(){
        return view('dashboard.caja.tipos_de_reporte');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Caja  $caja
     * @return \Illuminate\Http\Response
     */
    public function edit(Caja $caja)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Caja  $caja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Caja $caja)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Caja  $caja
     * @return \Illuminate\Http\Response
     */
    public function destroy(Caja $caja)
    {
        //
    }
}
