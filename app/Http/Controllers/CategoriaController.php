<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ComidaTipo;
use App\Imagen as ImgModel;
use DB;

use Carbon\Carbon;

use App\CategoriaImagen;
use Image;
class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if( $request->header('Authorization') ){
            $categorias = ComidaTipo::whereHas('imagenes')->with(['imagenes.imagen'])->get();
            return response()->json(['categorias' => $categorias]);
        }

        return view( 'dashboard.categorias.index', [
            'user' => auth()->user(),
            'categorias' => ComidaTipo::all()
        ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.categorias.create', [
            'user' => auth()->user(),
            'categorias' => ComidaTipo::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $categoria = ComidaTipo::create($request->all());

        if( $request->hasFile('imagen') )
            $this->storeImagen($request->imagen, $categoria);

        return redirect()->route( 'categorias', ['hasError' => false, 'id' => $categoria->id, 'action' => 'create'] );
    }

    protected function storeImagen($imagen, $comida){
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        CategoriaImagen::whereComidaTipoId($comida->id)->forceDelete();

        $newImgName = md5(Carbon::now()->format('Y-m-d H:i:s A').'#image'.$imagen->getSize().'#name'.$imagen->getClientOriginalName());
        $newDataImagen = [
            'mime' => $imagen->getClientOriginalExtension(),
            'nombre_original' => $imagen->getClientOriginalName(),
            'nombre' => $newImgName.'.'.$imagen->getClientOriginalExtension(),
            'ubicacion' => 'uploads/images',
            'size' => $imagen->getSize()
        ];

        $image = Image::make($imagen);
        $image->fit(400, 200);
        $image->save( public_path('uploads/images/'.$newImgName.'.'.$imagen->getClientOriginalExtension()) );

        $registroImagen = ImgModel::create( $newDataImagen );

        CategoriaImagen::create([
            'comida_tipo_id' => $comida->id,
            'imagen_id' => $registroImagen->id
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ComidaTipo = ComidaTipo::find($id);
        return view('dashboard.categorias.edit', [
            'user' => auth()->user(),
            'categoria' => $ComidaTipo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $ct = ComidaTipo::find($id);
        $this->storeImagen($request->imagen, $ct);
        $ct->fill( $request->only(['tipo', 'descripcion']) );
        $ct->save();

        return redirect()->route( 'categorias', ['hasError' => false,'id' => $ct->id, 'action' => 'update' ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comidaTipo = ComidaTipo::find($id);

        if( $comidaTipo ) $comidaTipo->delete();
        return response()->json( [ 'error' => false, 'message' => 'Registro removido correctamente' ] );
    }
}
