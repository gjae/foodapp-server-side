<?php

namespace App\Http\Controllers;

use App\ComboComida;
use Illuminate\Http\Request;
use App\ComboComida as Combos;
use App\ComboDetalle as CD;
use App\Http\Controllers\ComidaController;
use App\Comida;
use App\ComboImagen;
use App\Imagen as ImgModel;
use Image;
use Carbon\Carbon;
use DB;

class ComboComidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( !$request->ajax() ){
            $comida = new ComidaController;
            return view('dashboard.combos.index' , $comida->viewArray(['combos'=> Combos::all(), 'productos' => Comida::all() ]));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $comida = new ComidaController;
        return view('dashboard.combos.create', $comida->viewArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $combo = Combos::create($request->all());

            foreach( $request->comida_id as $key => $comida_id ){
                CD::create([
                    'comida_id' => $comida_id,
                    'cantidad_producto' => $request->cantidad_comida[$key],
                    'combo_comidas_id' => $combo->id
                ]);
            }

            if( $request->hasFile('imagen') ){
                $this->storeImagen( $request->imagen, $combo );
            }
            
            DB::commit();
            return redirect()->route( 'combos', ['hasError' => 'none', 'action' => 'create', 'id' => $combo->id] );
        }catch(\Exception $e){
            DB::rollback();
            return dd($e->getMessage());
        }
    }

    protected function storeImagen($imagen, $comida){

        $newImgName = md5(Carbon::now()->format('Y-m-d H:i:s A').'#image'.$imagen->getSize().'#name'.$imagen->getClientOriginalName());
        $newDataImagen = [
            'mime' => $imagen->getClientOriginalExtension(),
            'nombre_original' => $imagen->getClientOriginalName(),
            'nombre' => $newImgName.'.'.$imagen->getClientOriginalExtension(),
            'ubicacion' => 'uploads/images',
            'size' => $imagen->getSize()
        ];

        $image = Image::make($imagen);
        $image->fit(400, 200);
        $image->save( public_path('uploads/images/'.$newImgName.'.'.$imagen->getClientOriginalExtension()) );

        $registroImagen = ImgModel::create( $newDataImagen );

        ComboImagen::create([
            'combo_comida_id' => $comida->id,
            'imagen_id' => $registroImagen->id
        ]);
    }

    public function promos(Request $requuest){
        $cc = ComboComida::select('costo as price','combo_comidas.*')->get(['costo as price', 'combo_comidas.*']);

        return response()->json(["gg" => ".l."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ComboComida  $comboComida
     * @return \Illuminate\Http\Response
     */
    public function show(ComboComida $comboComida)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ComboComida  $comboComida
     * @return \Illuminate\Http\Response
     */
    public function edit(ComboComida $comboComida, $id)
    {
        $comida = new ComidaController;
        $comboComida = Combos::find($id);
        return view('dashboard.combos.edit', $comida->viewArray(['combo' => $comboComida]));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ComboComida  $comboComida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComboComida $comboComida)
    {
        DB::beginTransaction();
        try{
            $comboComida = Combos::find($request->combo_id);
            $comboComida->productos()->delete();

            foreach($request->only(['nombre', 'codigo', 'descripcion', 'costo', 'valido_desde', 'valido_hasta', 'cantidad_disponible', 'disponibilidad']) as $key => $value){
                $comboComida->$key = $value;
            }
            $comboComida->save();

            $detalles = CD::whereComboComidasId($request->combo_id)->onlyTrashed()->get();
            $fields = $request->comida_id;

            foreach($detalles as $detalle){
                foreach( $fields as $key => $value ){
                    if($detalle->comida_id == $value){
                        $detalle->restore();
                        $detalle->cantidad_comida = $request->cantidad_producto[$key];
                        $detalle->save();
                    }
                }
            }

            DB::commit();
            return redirect()->route( 'combos', ['hasError' => 'none', 'action' => 'update', 'id' => $comboComida->id] );
        //    return dd($comboComida);
        }catch(\Exception $e){
            DB::rollback();
            return dd($e->getMessage());

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ComboComida  $comboComida
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComboComida $comboComida, $id)
    {
        $comboComida = Combos::find($id);
        if( $comboComida )
            $comboComida->delete();

        return response()->json(['error' => false, 'message' => 'Combo suprimido correctamente']);
    }
}
