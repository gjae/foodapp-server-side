<?php

namespace App\Http\Controllers;

use App\Comida;
use App\ComidaTipo;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Imagen as ImgModel;
use App\ImagenComida;
use App\ComboComida;
use Image;
use Auth;

class ComidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( !$request->header('Authorization') )
            return view('dashboard.comidas.index', [
                'comidas' => Comida::all(),
                'user' => Auth::user()
            ]);
        if( $request->header('Authorization') || true ){
            $comidas = Comida::select('costo as price','comidas.*')
            ->whereHas('imagenes')->whereDisponible("SI")->with(['imagenes.imagen']);

            if( $request->has('category') ){
                $comidas = $comidas->whereComidaTipoId($request->category);
            }

            return response()->json([
                'comidas' => $comidas->get(),
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.comidas.create', 
            $this->viewArray([
                'tipos' => ComidaTipo::all()
            ] )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            $comida = new Comida($request->all());
            $comida->save();
            
            if( $request->hasFile('imagen') ){
                $this->storeImagen( $request->imagen, $comida );
            }
            
            return redirect()->route( 'comidas', [ 'hasError' => 'none', 'id' => $comida->id, 'type' => 'create'  ] );
        }catch( \Exception $e ){

            return dd($e->getMessage());
        }
    }

    protected function storeImagen($imagen, $comida){

        $newImgName = md5(Carbon::now()->format('Y-m-d H:i:s A').'#image'.$imagen->getSize().'#name'.$imagen->getClientOriginalName());
        $newDataImagen = [
            'mime' => $imagen->getClientOriginalExtension(),
            'nombre_original' => $imagen->getClientOriginalName(),
            'nombre' => $newImgName.'.'.$imagen->getClientOriginalExtension(),
            'ubicacion' => 'uploads/images',
            'size' => $imagen->getSize()
        ];

        $image = Image::make($imagen);
        $image->fit(400, 200);
        $image->save( public_path('uploads/images/'.$newImgName.'.'.$imagen->getClientOriginalExtension()) );

        $registroImagen = ImgModel::create( $newDataImagen );

        ImagenComida::create([
            'comida_id' => $comida->id,
            'imagen_id' => $registroImagen->id
        ]);
    }

    public function promos(Request $request){
        $cc = ComboComida::with(['imagenes.imagen'])->get(['costo as price', 'combo_comidas.*']);

        return response()->json($cc);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comida  $comida
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $comida = Comida::whereCodigo($codigo)->with(['medida', 'tipo'])->first();

        if( $comida )
            return response()->json(['registro' => $comida, 'error' => false]);

        return response()->json(['error' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comida  $comida
     * @return \Illuminate\Http\Response
     */
    public function edit(Comida $comida)
    {
        return view('dashboard.comidas.edit', $this->viewArray( ['comida' => $comida, 'tipos' => ComidaTipo::all()] ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comida  $comida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comida $comida)
    {   

        if( $request->hasFile('imagen') ){
            $img = ImagenComida::whereComidaId($comida->id)->first();
            if( $img ){
                $img->imagen()->delete();
                $img->delete();
            }
            $this->storeImagen($request->imagen, $comida);
        }

        foreach( $request->except(['_token' , '_method', 'user_id', 'imagen']) as $key => $value ){
            $comida->$key = $value;
        }
        $comida->save();
        return redirect()->route( 'comidas', [ 'hasError' => 'none', 'id' => $comida->id, 'type' => 'update' ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comida  $comida
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comida $comida)
    {
        $comida->delete();
        return response()->json(['error' => false,'message' => 'Registro suprimido correctamente']);
    }

    public function viewArray($extraData = []){
        $array = [
            'user' =>  Auth::user()
        ];

        $array = array_merge($array, $extraData);
        return $array;
    }

}
