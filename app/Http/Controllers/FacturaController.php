<?php

namespace App\Http\Controllers;

use App\Factura;
use Illuminate\Http\Request;
use App\Http\Controllers\ComidaController;
use App\DetalleFactura;
use DB;
use Carbon\Carbon;
class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->header('Authorization') ){

            $user = auth()->guard('api')->user();
            $pedidos = Factura::whereUserTitularId($user->id)
            ->with(['detalles.comida.imagenes.imagen', 'detalles.combos.imagenes.imagen'])->get();

            return response()->json( ['pedidos' =>$pedidos, 'error' => false] );
        }
        return view('dashboard.pedidos.index', [
            'user' => auth()->user(),
            'pedidos' => Factura::whereIn('estado_pedido', ['RECIBIDO', 'PROCESADO', 'PREPARADO'])->get()
        ]);
    }

    public function cancelar(Request $request){

        if( $request->header('Authorization') ){
            $user = auth()->guard('api')->user();
            $pedido = Factura::whereUserTitularId($user->id)
            ->whereId($request->pedido_id)->first();

            if(! $pedido){
                return response()->json([
                    'error' => true,
                    'message' => 'No se ha encontrado el registro solicitado, intente mas tarde.'
                ]);
            }

            $pedido->estado_pedido = 'CANCELADO';
            $pedido->save();
            return response()->json(['error' => false, 'message' => 'Pedido cancelado', 'pedido' => $pedido]);
        }

        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new ComidaController;
        return view('dashboard.pedidos.create', 
            $user->viewArray()
        );
    }

    protected function factura($request){
        $factura = new Factura($request->all());
        $factura->emision = Carbon::now();
        $factura->user_titular_id = auth()->guard('api')->user()->id;
        $factura->save();
        foreach( $request->detalles as $key => $detalle ){
            $factura->detalles()->save(
                new DetalleFactura($detalle)
            );
        }

        return response()->json(['error' => false, 'pedido' => Factura::whereId($factura->id)->with('detalles')->first()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( $request->ajax() || $request->header('Authorization') ){
            return $this->factura($request);
        }
        return response()->json([
            'error' => true,
            'message' => 'Error'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $factura = Factura::findOrFail($id);
        return view('dashboard.pedidos.show', [
            'user' => auth()->user(),
            'pedido' => $factura
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function edit(Factura $factura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $factura = Factura::find($id);
        $factura->user_registrador_id = $request->user_registrador_id;
        switch( $factura->estado_pedido){
            case 'RECIBIDO':
                $factura->estado_pedido = "PROCESADO";
            break;
            case 'PROCESADO':
                $factura->estado_pedido = 'PREPARADO';
            break;
            case 'PREPARADO':
                $factura->estado_pedido = 'LISTO';
            break;
            case 'LISTO':
                $factura->estado_pedido = 'ENTREGADO';
            break;
        }
        $factura->save();

        return redirect()->route('pedidos', ['hasError' => false, 'id' => $id, 'action' => 'update' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Factura $factura)
    {
        //
    }
}
