<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imagen extends Model
{
    use SoftDeletes;
    protected $table = 'imagens';
    protected $fillable = [
        'mime',
        'nombre_original',
        'nombre',
        'ubicacion',
        'size'
    ];

    public function comida(){
        return $this->hasOne('App\ImagenComida');
    }

    public function categorias(){
        return $this->hasMany('App\CategoriaImagen');
    }
}
