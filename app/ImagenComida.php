<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImagenComida extends Model
{
    use SoftDeletes;
    protected $table = 'imagen_comidas';
    protected $fillable = [
        'comida_id',
        'imagen_id'
    ];


    public function imagen(){
        return $this->belongsTo('App\Imagen');
    }

    public function comida(){
        return $this->belongsTo('App\Comida');
    }
}
