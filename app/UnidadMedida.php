<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnidadMedida extends Model
{
    use SoftDeletes;
    protected $table = 'unidad_medidas';
    protected $fillable = [
        'unidad',
        'codigo_unidad'
    ];

    protected $dates = ['deleted_at'];

    public function productos(){
        return $this->hasMany('App\Comida');
    }
}
