<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->date('fecha_nacimiento')->nullable();
            $table->string('email')->unique('UIN');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('network_id')->default('S/I');
            $table->enum('medio_registro', ['web', 'app', 'facebook', 'google', 'twitter'])
            ->default('app');
            $table->string('hometown')->nullable();
            $table->string('location')->nullable();
            $table->enum('rol', ['ADMIN','EMPLEADO', 'USUARIO'])->default('USUARIO');

            // EN EL CASO DE USAR FIREBASE A FUTURO: device_id DE FIREBASE
            // EN CASO DE USAR NODEJS , USAR COMO SOCKET_ID
            $table->string('device_id')->default('DID');

            $table->rememberToken();
            $table->timestamps();

            $table->index(['network_id', 'device_id'], 'DID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
