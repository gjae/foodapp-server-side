<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comidas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->decimal('costo' , 10,2)->default(0.00);
            $table->enum('disponible', ['SI', 'NO', 'AGOTADO'])
            ->default('SI');

            $table->integer('user_id')->unsigned();
            $table->integer('comida_tipo_id')->unsigned();
            $table->softDeletes();
            $table->datetime('add_at')->nullable();
            $table->string('codigo', 14)->default('N/A');

            $table->index(['codigo'], 'IND');

            $table->foreign('user_id')->references('id')
            ->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('comida_tipo_id')
            ->references('id')
            ->on('comida_tipos');
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comidas');
    }
}
