<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComboComidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combo_comidas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nombre');
            $table->text('descripcion')->nullable();

            $table->decimal('costo' , 10, 2)->default(0.00);
            $table->enum('disponibilidad',['DISPONIBLE','AGOTADO'])->default('DISPONIBLE');
            $table->datetime('valido_desde')->useCurrent();
            $table->softDeletes();

            // NULL QUIERE DECIR QUE EL COMBO ESTARA VIGENTE HASTA SER REMOVIDO 
            // O AGOTADO
            $table->datetime('valido_hasta')->nullable();
            $table->integer('cantidad_disponible')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combo_comidas');
    }
}
