<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            // FECHA Y HORA DE EMISION DE LA FACTURA
            $table->datetime('emision')->useCurrent();
            $table->decimal('impuesto', 10,2)->default(0.00);
            $table->decimal('subtotal', 10,2)->default(0.00);
            $table->decimal('total', 10,2)->default(0.00);
            $table->decimal('total_pagado', 10,2)->default(0.00);
            $table->decimal('total_cambio', 10,2)->default(0.00);
            $table->enum('medio_pago', ['EFECTIVO', 'CHEQUE', 'DEBITO', 'CREDITO'])
            ->default('EFECTIVO');
            $table->string('numero_medio', 42)->default('0000EFECTIVO');
            $table->integer('numero_factura')->default('0');
            $table->enum('estado_factura', ['EMITIDA', 'ANULADA', 'DECLARADA'])
            ->default('EMITIDA');

            $table->datetime('fecha_anulacion')->nullable();

            // TITULAR DE LA FACTURA
            $table->integer('user_titular_id')->unsigned();

            // USUARIO QUE REGISTRO LA FACTURA
            $table->integer('user_registrador_id')->unsigned()->nullable();

            $table->foreign('user_titular_id')
            ->references('id')
            ->on('users')->onDelete('restrict')->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
