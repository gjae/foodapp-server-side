<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('comida_id')->unsigned()->default(1);
            $table->integer('combo_comida_id')->unsigned()->default(1);
            $table->integer('cantidad')->default(1);
            $table->decimal('precio_unitario', 12,2)->default(0.00);
            $table->decimal('precio_total', 12,2)->default(0.00);
            $table->decimal('total_impuesto', 12,2)->default(0.00);
            $table->integer('factura_id')->unsigned();
            $table->softDeletes();

            $table->foreign('comida_id')
            ->references('id')->on('comidas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('combo_comida_id')
            ->references('id')->on('combo_comidas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factura_id')
            ->references('id')->on('facturas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_facturas');
    }
}
