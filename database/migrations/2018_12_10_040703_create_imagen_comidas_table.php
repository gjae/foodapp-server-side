<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenComidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagen_comidas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('imagen_id')->unsigned();
            $table->integer('comida_id')->unsigned();
            $table->softDeletes();

            $table->foreign('imagen_id')
            ->references('id')
            ->on('imagens')->onUpdate('cascade')->onDelete('cascade');
            
            $table->foreign('comida_id')
            ->references('id')
            ->on('comidas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagen_comidas');
    }
}
