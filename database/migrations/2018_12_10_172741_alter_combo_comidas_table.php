<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterComboComidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('combo_comidas', function (Blueprint $table) {
            $table->string('codigo', 12)->default('NA');

            $table->index(['codigo'], 'CCI');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('combo_comidas', function (Blueprint $table) {
            $table->dropIndex(['CCI']);
            $table->dropColumn(['codigo']);
        });
    }
}
