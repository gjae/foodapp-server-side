<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadMedidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidad_medidas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('unidad', 50);
            $table->string('codigo_unidad', 6)->default('NA');

            $table->index(['codigo_unidad'], 'CUI');
        });

        \DB::table('unidad_medidas')->insert([
            'unidad' => 'DEFAULT',
            'deleted_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'codigo_unidad' => '00DEFT'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidad_medidas');
    }
}
