<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('imagen_id')->unsigned();
            $table->integer('comida_tipo_id')->unsigned();

            $table->foreign('imagen_id')->references('id')->on('comidas')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('comida_tipo_id')->references('id')->on('comida_tipos')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria_imagens');
    }
}
