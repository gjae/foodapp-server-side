<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComboImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combo_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('imagen_id')->unsigned();
            $table->integer('combo_comida_id')->unsigned();
            $table->softDeletes();

            $table->foreign('imagen_id')
            ->references('id')
            ->on('imagens')->onUpdate('cascade')->onDelete('cascade');
            
            $table->foreign('combo_comida_id')
            ->references('id')
            ->on('comidas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combo_imagens');
    }
}
