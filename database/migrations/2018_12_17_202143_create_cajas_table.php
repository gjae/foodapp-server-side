<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cajas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->datetime('caja_dia')->nullable();
            $table->datetime('fecha_hora_apertura')->nullable();
            $table->datetime('fecha_hora_cierre')->nullable();

            $table->decimal('importe_apertura', 12,2)->default(0.00);
            $table->decimal('importe_cierre', 12, 2)->default(0.00);
            $table->integer('user_id')->unsigned();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cajas');
    }
}
