<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajaDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caja_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->decimal('total', 12,2)->default(0.00);
            $table->integer('factura_id')->unsigned();
            $table->integer('caja_id')->unsigned();
            $table->enum('proceso', ['CIERRE', 'APERTURA'])->default('CIERRE');

            $table->foreign('factura_id')
            ->references('id')->on('facturas')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('caja_id')
            ->references('id')->on('cajas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caja_detalles');
    }
}
