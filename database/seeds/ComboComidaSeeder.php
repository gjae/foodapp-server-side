<?php

use Illuminate\Database\Seeder;

class ComboComidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('combo_comidas')->insert([
            'nombre' => 'DEFAULT',
            'disponibilidad' => 'AGOTADO',
            'deleted_at' => '2018-12-31 00:00:00',
            'disponibilidad' => 'AGOTADO',
            'valido_desde' => date('Y-m-d H:i'),
            'valido_hasta' => date('Y-m-d H:i'),
        ]);
    }
}
