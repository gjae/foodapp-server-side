<?php

use Illuminate\Database\Seeder;

class ComidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('comidas')->insert([
            'nombre' => 'DEFAULT',
            'descripcion' => 'DEFAULT',
            'costo' => 0.00,
            'disponible' => 'NO',
            'user_id' => 1,
            'comida_tipo_id' => 1,
            'add_at' => date('Y-m-d H:i'),
            'codigo' => 'DEFAULT',
            'unidad_medida_id' => 1
        ]);
    }
}
