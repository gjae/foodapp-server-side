<?php

use Illuminate\Database\Seeder;
class ComidaTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = [
            'viveres' => 'Viveres varios',
            'bebidas' => 'Productos de tipo bebida',
            'rapida' => 'Comida rapida',
            'pizzas' => 'Pizzas',
            'arroz' =>  'Comidas a base de arroz',
            'sopas' => 'Sopas varias',
            'cremas' => 'Cremas'
        ];

        foreach($tipos as $key => $value){
            \DB::table('comida_tipos')->insert([
                'tipo' => $key,
                'descripcion' => $value
            ]);
        }
    }
}
