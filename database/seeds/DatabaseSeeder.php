<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    private $seeders = [
        UsersTableSeeder::class,
        UnidadMedidaSeeder::class,
        ComidaTipoSeeder::class,
        ComidaSeeder::class,
        ComboComidaSeeder::class
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call($this->seeders);
    }
}
