<?php

use Illuminate\Database\Seeder;

class UnidadMedidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unidades = [
            'KILOGRAMOS' => 'KG',
            'LITROS' => 'LTS',
            'CAJA' => 'CJ',
            'TONELADA' => 'TNLDS',
            'MILILITROS' => 'MLTS',
            'GRAMOS' => 'GR',
            'MILIGRAMOS' => 'MGRS',
            'ONZAS' => 'OZ',
            'BARRIL' => 'BR',
            'ROLLO' => 'RLL',
            'UNIDAD' => 'UND',
            'LIBRA' => 'LBR'
        ];
        foreach($unidades as $unidad => $codigo){
            \DB::table('unidad_medidas')->insert([
                'unidad' => $unidad,
                'codigo_unidad' => $codigo
            ]);
        }
    }
}
