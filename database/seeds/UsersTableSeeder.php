<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'Admin',
            'lastname' =>'Admin',
            'fecha_nacimiento' => date('Y-m-d'),
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456')
        ]);
    }
}
