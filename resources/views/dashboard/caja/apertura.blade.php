@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Gestion de categorias')
@section('title_for_wrapper', 'Categorias')
@section('panel_header', 'Categorias disponibles')

<div class="container">
    @section('dash_content')
        <form action="{{url('dashboard/caja')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-sm-4 col-lg-4 col-md-4 col-sm-offset-4">
                    <label for="">Monto de apertura</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <strong>$. </strong>
                        </span>
                        <input type="text" data-thousands="" value="0.00" style="text-align: right;" name="importe_apertura" required="required" class="form-control moneda">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <br>
                    <hr>
                    <button class="btn btn-success btn-block">Guardar</button>
                </div>
            </div>
        </form>
    @endsection
</div>
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.maskMoney.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.table1').DataTable();
    $(".moneda").maskMoney();
  });

  function deleteRow(e, btn){
    if( confirm('¿Seguro de que desea realizar esta accion?') ){
        url = btn.getAttribute('url');
        $.post(url, {"_method": "DELETE", "_token": "{{ csrf_token() }}"}, function(response){
            alert(response.message);
            location.reload();
        });
      }
  }

</script>

@endsection