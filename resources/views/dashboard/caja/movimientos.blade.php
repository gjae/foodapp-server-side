@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Gestion de categorias')
@section('title_for_wrapper', 'Categorias')
@section('panel_header', 'Categorias disponibles')

<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-4 col-lg-4 col-md-4">
                <a href="{{ url("dashboard/caja/$caja->id") }}" class="btn btn-app accion-cierre">
                    <i class="fa  fa-clock-o"></i>
                    Cerrar caja
                </a>
            </div>
        </div>
        <div class="row"> 
            <div class="col-sm-12 col-lg-12 col-md-12">
                <table class="table table-stripped table1">
                    <thead>
                        <tr>
                            <th>ID #</th>
                            <th>Emision</th>
                            <th>Titular</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($facturas as $factura)
                            <tr>
                                <td>{{$factura->id}}</td>
                                <td>{{$factura->emision->format('Y-m-d')}}</td>
                                <td>{{$factura->titular->name.' '.$factura->titular->lastname}}</td>
                                <td>{{$factura->total}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endsection
</div>
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.table1').DataTable();
    $(".accion-cierre").on('click', function(event){
        event.preventDefault();
        if( confirm('¿Seguro que desea caja?') ){
            window.open( $(this).attr('href'), '_blank' );
        }
    });
  });

  function deleteRow(e, btn){
    if( confirm('¿Seguro de que desea realizar esta accion?') ){
        url = btn.getAttribute('url');
        $.post(url, {"_method": "DELETE", "_token": "{{ csrf_token() }}"}, function(response){
            alert(response.message);
            location.reload();
        });
      }
  }


</script>

@endsection