<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>REPORTE DE CIERRE POR DIA</title>
</head>
<body>
    <style>
        *{
            border: 0;
        }
        body{
            font-family: sans-serif;
        }
        .header{
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
        }
        .business-data [class^="line-"]{
            text-align: center;
            margin: 0;
        }
    </style>
    
    <header>
        @include('dashboard.caja.reportes.header')
    </header>

    <main>

        @foreach( $dia->detalles as $detalle )                    

            <table width="100%" border="1" cellspacing="0" >            
                <tr class="footer">
                    <th>
                        Factura: {{ $detalle->factura->id }}
                    </th>
                    <th>&nbsp;</th>
                    <th>
                            Fecha: {{$detalle->factura->emision->format('Y-m-d')}}
                    </th>
                    <th>
                         Hora: {{$detalle->factura->emision->format('H:i')}}
                    </th>
                    <th>
                        Total:
                    </th>
                    <th align="right">
                        {{$detalle->factura->total}}
                    </th>
                </tr>
                <thead class="header">
                    <tr>
                       <th>Codigo</th>
                       <th>Nombre</th>
                       <th>Descripcion</th>
                       <th>Cantidad</th>
                       <th>P. Unitario</th>
                       <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $total =0;
                    @endphp
                    @foreach( $detalle->factura->detalles as $f_detalle )
                       @php
                            $comida = $f_detalle->comida_id == 1 ? $f_detalle->combos : $f_detalle->comida;   
                            $total += $f_detalle->precio_total;
                       @endphp
                        <tr>
                            <td>
                                {{$comida->codigo}}
                            </td>
                            <td>
                                {{$comida->nombre}}
                            </td>
                            <td>
                                {{$comida->descripcion}}
                            </td>
                            <td>
                                {{$f_detalle->cantidad}}
                            </td>
                            <td align="right">
                                {{$f_detalle->precio_unitario}}
                            </td>
                            <td align="right">
                                {{$f_detalle->precio_total}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
            <br>
            <br>
            <br>
        @endforeach
        
    </main>

</body>
</html>