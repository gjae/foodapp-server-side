@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Caha')
@section('title_for_wrapper', 'Resumenes de caja')
@section('panel_header', 'Resumenes totales de caja')

<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-4 col-lg-4 col-md-4">
                <a href="{{route('tipos_reportes_caja')}}" class="btn btn-app repores-caja">
                    <i class="fa  fa-print"></i>
                    Reportes
                </a>
            </div>
        </div>
        <div class="row"> 
            <div class="col-sm-12 col-lg-12 col-md-12">
                <table class="table table-stripped table1">
                    <thead>
                        <tr>
                            <th>ID #</th>
                            <th>Fecha y hora de apertura</th>
                            <th>Fecha y hora de cierre</th>
                            <th>Monto de apertura</th>
                            <th>Monto de cierre</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dias_caja as $key => $dia)
                            <tr>
                                <td>{{$key}}</td>
                                <td>{{$dia->fecha_hora_apertura->format('Y-m-d H:i')}} </td>
                                <td>{{$dia->fecha_hora_cierre->format('Y-m-d H:i')}}</td>
                                <td>{{$dia->importe_apertura}}</td>
                                <td>{{$dia->importe_cierre}}</td>
                                <th>
                                    <a href="" onclick="reportes(event, this)" data="{{url('dashboard/caja/reportes/dia', $dia->id)}}" class="btn btn-success">
                                        <i class="fa fa-print"></i>
                                    </a>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endsection
</div>
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.table1').DataTable();
    $(".repores-caja").on('click', function(event){
        event.preventDefault();
        window.open( $(this).attr('href'), 'Tipos de reportes', 'height=400,width=450' );
    });
  });

  function deleteRow(e, btn){
    if( confirm('¿Seguro de que desea realizar esta accion?') ){
        url = btn.getAttribute('url');
        $.post(url, {"_method": "DELETE", "_token": "{{ csrf_token() }}"}, function(response){
            alert(response.message);
            location.reload();
        });
      }
  }

  function reportes(e, a){
      e.preventDefault();
      let url = a.getAttribute('data');
      window.open(url, "Reportes", "width=800,height=800");
  }


</script>

@endsection