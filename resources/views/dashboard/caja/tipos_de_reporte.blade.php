<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">  <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <title>Tipos de reportes de caja</title>
</head>
<body>
 
    <style>
        body{
            box-sizing: border-box;
        }
        .area-select{
            background: #e3e3e3;
            height: 400px;
            width: 50%;
            border-radius: 10px;
            margin-left: 25%;
            margin-right: 50%;
        }
    </style>
    

    <section class="area-select">
        <form action="">
            <div class="">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <label for="">Fecha desde</label>
                        <input type="date" required="required" name="inicio" id="inicio" class="form-control">
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <label for="">Fecha hasta</label>
                        <input type="date" required="required" name="fin" id="inicio" class="form-control">
                    </div>
                </div>
            </div>
        </form>
    </section>


</body>
</html>