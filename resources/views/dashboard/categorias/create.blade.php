@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Crear nueva categoría')
@section('title_for_wrapper', 'Nueva')
@section('panel_header', 'Categorias - Nuevo')

<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <form action="{{ route('categorias') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value="{{ auth()->user()->id }}" name="user_id" id="user_id">
                    <div class="row">
                        <div class="col-sm-3 col-lg-3 col-md-3">
                            <label for="">Categoría</label>
                            <input type="text" required placeholder="Ej: Pollo" class="form-control" name="tipo" id="tipo">
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <label for="">Imagen de la categoria</label>
                            <input type="file" required="required" name="imagen" id="imagen" class="form-control">
                        </div>
                        <div class="col-sm-6 col-lg-6 col-md-6">
                            <label for="">Descripción</label>
                            <input type="text" placeholder="Breve descripción de la categoria" name="descripcion" required="required" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <br>
                        <hr>
                        <div class="col-sm-12 col-lg-12 col-md-12">
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </div>
                </form>  
            </div>
        </div>
    @endsection
</div>
@section('jquery')
<script src="{{ asset('plugins/input-mask/jquery.maskMoney.min.js') }}"></script>
<script>
$(document).ready(function(){

    $("#costo").maskMoney();
})

</script>

@endsection