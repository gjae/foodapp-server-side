@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Gestion de categorias')
@section('title_for_wrapper', 'Categorias')
@section('panel_header', 'Categorias disponibles')

<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-4 col-lg-4 col-md-4">
                <a href="{{ route('categorias.create') }}" class="btn btn-app">
                    <i class="fa fa-plus"></i>
                    Agregar una
                </a>
            </div>
        </div>
        <div class="row"> 
            <div class="col-sm-12 col-lg-12 col-md-12">
                <table class="table table-stripped table1">
                    <thead>
                        <tr>
                            <th>ID #</th>
                            <th>Descripcion</th>
                            <th>Categoria</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categorias as $categoria)
                            <tr>
                                <td>{{$categoria->id}}</td>
                                <td>{{$categoria->descripcion}}</td>
                                <td>{{$categoria->tipo}}</td>
                                <td>
                                    <button class="btn btn-danger" onclick="deleteRow(event, this)" url="{{ url("dashboard/categorias/$categoria->id") }}">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                    <a class="btn btn-warning" href="{{ url("dashboard/categorias/$categoria->id/edit") }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endsection
</div>
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.table1').DataTable();
  });

  function deleteRow(e, btn){
    if( confirm('¿Seguro de que desea realizar esta accion?') ){
        url = btn.getAttribute('url');
        $.post(url, {"_method": "DELETE", "_token": "{{ csrf_token() }}"}, function(response){
            alert(response.message);
            location.reload();
        });
      }
  }

</script>

@endsection