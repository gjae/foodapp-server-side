@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Crear nuevo producto')
@section('title_for_wrapper', 'Nuevo')
@section('panel_header', 'Productos, servicios y comidas - Nuevo')
@section('_css')

<link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
@endsection
<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <form action="{{ url('dashboard/combos', $combo->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" value="{{ auth()->user()->id }}" name="user_id" id="user_id">
                    <input type="hidden" name="combo_id" value="{{$combo->id}}">
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <label for="">
                            Nombre
                            
                        </label>
                        <input type="text" value="{{$combo->nombre}}" required placeholder="Nombre del registro" class="form-control" name="nombre" id="nombre">
                    </div>
                    <div class="col-sm-2 col-lg-2 col-md-2">
                        <label for="">
                            Codigo
                            
                        </label>
                        <input type="text" value="{{ $combo->codigo}}" class="form-control" name="codigo" required="required" placeholder="Codigo" id="codig">
                    </div>
                    <div class="col-sm-2 col-lg-2 col-md-2">
                        <label for="">Precio</label>
                        <input type="text"  value="{{ $combo->costo}}" data-thousands="" value="0.00" style="text-align: right;" class="form-control" name="costo" required="required" placeholder="Precio del item" id="costo">
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="form-group">
                            <label> Valido desde / hasta</label>
                
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="hidden" value="{{ $combo->valido_desde->format('Y-m-d H:i') }}" id="valido_desde" name="valido_desde" value="{{ \Carbon\Carbon::now()->format('Y-m-d H:i:s') }}">
                                <input type="hidden" value="{{ $combo->valido_hasta->format('Y-m-d H:i:s') }}" id="valido_hasta" name="valido_hasta" value="{{ \Carbon\Carbon::now()->format('Y-m-d H:i:s') }}">
                                <input type="text" value="{{ $combo->valido_desde->format('Y-m-d H:i')}} - {{ $combo->valido_hasta->format('Y-m-d H:i:s') }}" required name="tiempo" class="form-control pull-right" id="reservation">
                            </div>
                                <!-- /.input group -->
                        </div>
                    </div>
                    <div class="col-sm-2 col-lg-2 col-md-2">
                        <label for="">Disponibilidad</label>
                        <input type="number" value="{{ $combo->cantidad_disponible}}" class="form-control" name="cantidad_disponible" placeholder="Cantidad disponible" id="disponibilidad">
                    </div>
                    <div class="col-sm-2 col-lg-2 col-lg-3">
                        <label for="">¿Disponible?</label>
                        <select name="disponibilidad" id="disponibilidad" class="form-control" required>
                            <option value="DISPONIBLE" {{$combo->disponibilidad == "DISPONIBLE"? 'selected' :'' }}>
                                Disponible
                            </option>
                            <option value="AGOTADO" {{$combo->disponibilidad == "AGOTADO"? 'selected' :'' }}>
                                Agotado
                            </option>
                        </select>
                    </div>
                    <div class="col-sm-8 col-md-8 col-lg-8">
                        <label for="">Descripcion</label>
                        <input type="text" value="{{$combo->descripcion}}" class="form-control" name="descripcion" placeholder="Agrega una descripcion del producto" id="descripcion">
                    </div>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <h3 class="page-header">
                            ¿Qué productos hay dentro del combo?
                        </h3>
                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Descripcion</th>
                                    <th width="3%">Cantidad</th>
                                    <th style="align-items:center;" >
                                        Incluir
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( \App\Comida::all() as $key => $producto )
                                    @php
                                        $detalle = \App\ComboDetalle::whereComidaId($producto->id)->first();
                                    @endphp
                                    <tr>
                                        <th>{{ $producto->codigo }}</th>
                                        <th>{{ $producto->nombre }}</th>
                                        <th>{{$producto->descripcion}}</th>
                                        @if( !is_null($detalle ) && $detalle->comida_id == $producto->id )
                                            <th width="3%">
                                                <input value="{{$detalle->cantidad_comida}}" type="text" data-symbol="{{ $producto->medida->codigo_unidad }} " data-thousands=""   style="text-align: center;" value="0" class="form-control producto cantidades" name="cantidad_producto[]" id="{{$key}}">   
                                            </th>
                                            <th style="align-items:center;">
                                                <input checked originalCantidad="{{$detalle->cantidad_comida}}" data-input="{{$key}}" value="{{ $producto->id }}" onclick="inputEvent(event, this)"  type="checkbox" name="comida_id[]">
                                            </th>
                                            @else
                                            <th width="3%">
                                                <input  type="text" data-symbol="{{ $producto->medida->codigo_unidad }} " data-thousands="" value="0.00"  style="text-align: center;" disabled readonly value="0" class="form-control producto cantidades" name="cantidad_producto[]" id="{{$key}}">   
                                            </th>
                                            <th style="align-items:center;">
                                                <input originalCantidad="0" data-input="{{$key}}" value="{{ $producto->id }}" onclick="inputEvent(event, this)"  type="checkbox" name="comida_id[]">
                                            </th>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <br>
                        <button type="submit" class="btn btn-success">
                            <i class="glyphicon glyphicon-ok"></i>
                            Guardar
                        </button>
                    </div>
                </form>  
            </div>
        </div>
    @endsection
</div>
@section('jquery')
<script src="{{ asset('plugins/input-mask/jquery.maskMoney.min.js') }}"></script>

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
$(document).ready(function(){
    let table = $(".table").DataTable();
    $("#costo").maskMoney();
    $(".cantidades").maskMoney();
    $('#reservation').daterangepicker({ 
        timePicker: true, 
        timePickerIncrement: 30, 
        format: 'YYYY-MM-DD H:mm ' ,
        locale: {
            format: 'YYYY-MM-DD H:mm'
        },
        timePickerIncrement: 5,
        timePicker24Hour: true
    }, (start, end, label) =>{
        console.log(start, end, label);
        $("#valido_desde").val(start.format('YYYY-MM-DD H:mm:ss'));
        $("#valido_hasta").val(end.format('YYYY-MM-DD H:mm:ss'));
    });
})

function inputEvent(e, checkbox){
    let input = document.getElementById( checkbox.getAttribute('data-input') );
    if( checkbox.checked ){
        
        input.readOnly = false;
        input.disabled = false;
        input.value = checkox.getAttribute('originalCantidad');
    }else{
        input.value = 0;
        input.readOnly = true;
        input.disabled = true;
    }

}

</script>

@endsection