@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Crear nuevo producto')
@section('title_for_wrapper', 'Nuevo')
@section('panel_header', 'Productos, servicios y comidas - Nuevo')

<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <form action="{{ route('comidas') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value="{{ auth()->user()->id }}" name="user_id" id="user_id">
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <label for="">
                            Nombre
                            
                        </label>
                        <input type="text" required placeholder="Nombre del registro" class="form-control" name="nombre" id="nombre">
                    </div>
                    <div class="col-sm-2 col-lg-2 col-md-2">
                        <label for="">
                            Codigo
                            
                        </label>
                        <input type="text" class="form-control" name="codigo" required="required" placeholder="Codigo" id="codig">
                    </div>
                    <div class="col-sm-2 col-lg-2 col-md-2">
                        <label for="">Precio</label>
                        <input type="text"  data-thousands="" value="0.00" style="text-align: right;" class="form-control" name="costo" required="required" placeholder="Precio del item" id="costo">
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <label for="">
                            Tipo de producto
                        </label>
                        <select name="comida_tipo_id" id="comida_tipo_id" class="form-control">
                            @foreach( $tipos as $key => $tipo )
                                <option value="{{$tipo->id}}">
                                    {{ $tipo->descripcion }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-">
                        <label for="">Medida</label>
                        <select name="unidad_medida_id" id="" class="form-control" required="required">
                            <option value="" selected disabled> -- SELECCIONE UNO --</option>
                            @foreach(\App\UnidadMedida::all() as $key => $unidad)
                                <option value="{{$unidad->id}}">{{$unidad->codigo_unidad}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <label for="">Imagen de muestra</label>
                        <input type="file" required class="form-control" name="imagen" id="imagen">
                    </div>
                    <div class="col-sm-8 col-md-8 col-lg-8">
                        <label for="">Descripcion</label>
                        <input type="text" class="form-control" name="descripcion" placeholder="Agrega una descripcion del producto" id="descripcio">
                    </div>
                    <hr>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <br>
                        <button type="submit" class="btn btn-success">
                            <i class="glyphicon glyphicon-ok"></i>
                            Guardar
                        </button>
                    </div>
                </form>  
            </div>
        </div>
    @endsection
</div>
@section('jquery')
<script src="{{ asset('plugins/input-mask/jquery.maskMoney.min.js') }}"></script>
<script>
$(document).ready(function(){

    $("#costo").maskMoney();
})

</script>

@endsection