@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Actualizar producto')
@section('title_for_wrapper', 'Editar y/o Actualizar')
@section('panel_header', 'Productos, servicios y comidas - Edición / Actualización')

<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <form action="{{ url('dashboard/comidas', $comida->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="hidden" value="{{ auth()->user()->id }}" name="user_id" id="user_id">
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <label for="">
                            Nombre
                            
                        </label>
                        <input type="text" value="{{ $comida->nombre }}"  required placeholder="Nombre del registro" class="form-control" name="nombre" id="nombre">
                    </div>
                    <div class="col-sm-2 col-lg-2 col-md-2">
                        <label for="">
                            Codigo
                            
                        </label>
                        <input type="text" value="{{ $comida->codigo }}"   class="form-control" name="codigo" required="required" placeholder="Codigo" id="codig">
                    </div>
                    <div class="col-sm-2 col-lg-2 col-md-2">
                        <label for="">Precio</label>
                        <input type="text" value="{{ $comida->costo }}"    data-thousands="" value="0.00" style="text-align: right;" class="form-control" name="costo" required="required" placeholder="Precio del item" id="costo">
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <label for="">
                            Tipo de producto
                        </label>
                        <select name="comida_tipo_id" id="comida_tipo_id" class="form-control">
                            @foreach( $tipos as $key => $tipo )
                                <option {{ $comida->comida_tipo_id == $tipo->id ? 'selected': '' }} value="{{$tipo->id}}">
                                    {{ $tipo->descripcion }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <label for="">Estado del producto</label>
                        <select name="" id="disponible" class="form-control" namme="disponible">
                            <option {{ $comida->disponible == "SI" ? 'selected' : '' }} value="SI"> SI </option>
                            <option {{ $comida->disponible == "NO" ? 'selected' : '' }} value="SI"> NO </option>
                            <option {{ $comida->disponible == "AGOTADO" ? 'selected' : '' }} value="SI"> AGOTADO </option>
                        </select>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-">
                        <label for="">Medida</label>
                        <select name="unidad_medida_id" id="" class="form-control" required="required">
                            <option value="" selected disabled> -- SELECCIONE UNO --</option>
                            @foreach(\App\UnidadMedida::all() as $key => $unidad)
                                <option {{ $comida->unidad_medida_id == $unidad->id ? 'selected' : '' }} value="{{$unidad->id}}">
                                    {{$unidad->codigo_unidad}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 col-lg-2 col-md-2">
                            <label for="">Imagen de muestra</label>
                            <input type="file" class="form-control" name="imagen" id="imagen">
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <label for="">Descripcion</label>
                        <input type="text" value="{{ $comida->descripcion }}"   class="form-control" name="descripcion" placeholder="Agrega una descripcion del producto" id="descripcio">
                    </div>
                    <hr>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <br>
                        <button type="submit" class="btn btn-success">
                            <i class="glyphicon glyphicon-ok"></i>
                            Guardar
                        </button>
                    </div>
                </form>  
            </div>
        </div>
    @endsection
</div>
@section('jquery')
<script src="{{ asset('plugins/input-mask/jquery.maskMoney.min.js') }}"></script>
<script>
$(document).ready(function(){

    $("#costo").maskMoney();
})

</script>

@endsection