@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Gestion de productos / carta')
@section('title_for_wrapper', 'Productos, servicios y comidas')
@section('panel_header', 'Productos, servicios y comidas')

<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-4 col-lg-4 col-md-4">
                <a href="{{ route('comidas.create') }}" class="btn btn-app">
                    <i class="fa fa-plus"></i>
                    Agregar una
                </a>
            </div>
        </div>
        <div class="row"> 
            <div class="col-sm-12 col-lg-12 col-md-12">
                <table class="table table-stripped table1">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>¿Disponible?</th>
                            <th>Tipo</th>
                            <th>Costo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($comidas as $key => $comida)
                            <tr>
                                <td>
                                    {{ $comida->codigo }}
                                </td>
                                <td>
                                    {{ $comida->nombre }}
                                </td>
                                <td>
                                    {{$comida->descripcion}}
                                </td>
                                <td>
                                    <div class="label {{ $comida->label() }}">
                                        {{$comida->disponible}}
                                    </div>
                                </td>
                                <td>
                                    {{$comida->tipo->tipo}}
                                </td>
                                <td>
                                    {{ $comida->costo }}
                                </td>
                                <td>
                                    <a href="{{ url("dashboard/comidas/$comida->id/edit") }}" class="btn btn-warning">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>
                                    <button url="{{ url("dashboard/comidas/$comida->id") }}" onclick="deleteRow(event, this)" class="btn btn-danger delete">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endsection
</div>
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.table1').DataTable();
  });

  function deleteRow(e, btn){
    if( confirm('¿Seguro de que desea realizar esta accion?') ){
        url = btn.getAttribute('url');
        $.post(url, {"_method": "DELETE", "_token": "{{ csrf_token() }}"}, function(response){
            alert(response.message);
            location.reload();
        });
      }
  }

</script>

@endsection