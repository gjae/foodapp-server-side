@extends('layouts.dashboard_app_control')
@section('title', 'Bienvenido al dashboard')
@extends('layouts.dashboard_app_control')
@section('title', 'Dashboard de control de app')
@section('title_for_wrapper', 'Bienvenido')
@section('panel_header', 'Version 1.0.0')

<div class="container">
    @section('dash_content')
        <h3 class="page-header">
            ¡Estamos en la version 1.0.0!
        </h3>
        <p>
            En esta version encontraremos la parte mas basica de toda la app, aquì encontraremos todo lo basico para el manejo
            del contenido de la aplicación, ademas podras gestionar los clientes, igualmente incluimos un metodo de facturación,
            el objetivo es generar un sistema <strong>Todo en uno</strong> para gestionar todo lo requerido en un establecimiento
            de comida.
        </p>
    @endsection
</div>