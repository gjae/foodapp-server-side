@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Nuevo pedido')
@section('title_for_wrapper', 'Pedidos')
@section('panel_header', 'Nuevo pedido')

<div class="container">
    @section('dash_content')
        <form action="">
            <div class="row">
                <div class="col-sm-5 col-lg-5 col-md-5">
                    <label for="">Codigo</label>
                    <input type="text" onkeyup="buscarProducto(event, this)" name="codigo" id="codigo" class="form-control">
                </div>
            </div>
        </form>
    @endsection
</div>
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.table1').DataTable();
  });

  function deleteRow(e, btn){
    if( confirm('¿Seguro de que desea realizar esta accion?') ){
        url = btn.getAttribute('url');
        $.post(url, {"_method": "DELETE", "_token": "{{ csrf_token() }}"}, function(response){
            alert(response.message);
            location.reload();
        });
      }
  }

  function buscarProducto(e, input){
      let url = location.protocol+'//'+location.host+'/dashboard/comidas/'+input.value;
      if( input.value.length >= 6 ){
          $.getJSON(url, {}, function(response){
              if(!response.error){
                  let row = `
                    <tr>
                        <input type="hidden" name="producto_id[]" value="${response.registro.id}" />
                        <td>${response.registro.codigo}</td>
                        <td>${response.registro.nombre}</td>
                        <td>${response.registro.costo}</td>
                    </tr>
                  `
              }
          });
      }
  }

</script>

@endsection