@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Gestion de categorias')
@section('title_for_wrapper', 'Categorias')
@section('panel_header', 'Categorias disponibles')

<div class="container">
    @section('dash_content')
        <div class="row">
            <div class="col-sm-4 col-lg-4 col-md-4">
                <a href="{{ route('categorias.create') }}" class="btn btn-app">
                    <i class="fa fa-plus"></i>
                    Agregar una
                </a>
            </div>
        </div>
        <div class="row"> 
            <div class="col-sm-12 col-lg-12 col-md-12">
                <table class="table table-stripped table1">
                    <thead>
                        <tr>
                            <th>ID #</th>
                            <th>Fecha</th>
                            <th>Total</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pedidos as $pedido)
                            <tr style="text-align: center;">
                                <td>{{$pedido->id}}</td>
                                <td>{{$pedido->emision->format('Y-m-d H:i')}}</td>
                                <td>{{$pedido->total}}</td>
                                <td>{{$pedido->titular->name.' '.$pedido->lastname}}</td>
                                <td>
                                    <div class="label label-success">
                                        {{$pedido->estado_pedido}}
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ route('pedidos')."/$pedido->id" }}" class="btn btn-success">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endsection
</div>
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.table1').DataTable();
  });

  function deleteRow(e, btn){
    if( confirm('¿Seguro de que desea realizar esta accion?') ){
        url = btn.getAttribute('url');
        $.post(url, {"_method": "DELETE", "_token": "{{ csrf_token() }}"}, function(response){
            alert(response.message);
            location.reload();
        });
      }
  }

</script>

@endsection