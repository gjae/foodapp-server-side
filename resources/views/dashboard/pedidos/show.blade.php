@extends('layouts.dashboard_app_control')
@extends('layouts.dashboard_app_control')
@section('title', 'Carta')
@extends('layouts.dashboard_app_control')
@section('title', 'Detalles de pedido')
@section('title_for_wrapper', 'Pedido')
@section('panel_header', 'Detalles de pedido')

<div class="container">
    @section('dash_content')
        <form action="{{ route("pedidos.update", [$pedido->id]) }}" method="post">
            @csrf
            @method('PUT')
            <input type="hidden" name="user_registrador_id" value="{{$user->id}}" id="">
            <div class="row">
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <a href="{{ route("pedidos") }}" class="btn btn-danger">
                        Volver atras
                    </a>
                </div>
                <div class="col-sm-2 col-lg-2 col-md-2 col-md-offset-4 col-lg-offset-8">
                    <button class="btn btn-success">
                        Procesar pedido
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <label for="">Usuario</label>
                    <input type="text" class="form-control" value="{{$pedido->titular->name.' '.$pedido->titular->lastname}}" disabled="disabled" readonly="readonly" id="user">   
                </div>
                <div class="col-sm-2 col-lg-2 col-md-2">
                    <label for="">Fecha y hora</label>
                    <input type="text" value="{{$pedido->emision->format('Y-m-d H:i')}}" class="form-control" disabled="disabled" readonly="readonly" id="fecha">
                </div>
                <div class="col-sm-2 col-lg-2 col-md-2">
                    <label for=""># de pedido</label>
                    <input type="text" value="{{$pedido->id}}" class="form-control" readonly="readonly" disabled="disabled" id="numero">
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <label for="">Estado</label>
                    <input type="text" value="{{$pedido->estado_pedido}}" readonly="readonly" disabled="disabled" class="form-control">
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <label for="">Subtotal</label>
                    <input type="text" value="{{$pedido->subtotal}}" readonly="readonly" disabled="disabled" class="form-control">
                </div>
                <div class="col-sm-2 col-lg-2 col-md-2">
                    <label for="">Total</label>
                    <input type="text" value="{{$pedido->total}}" readonly="readonly" class="form-control" disabled="disabled" class="form-contro">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <h3 class="page-header">Detalles</h3>
                </div>
            </div>
            <div class="row-col-md-12 col-lg-12 col-sm-12">
                <table class="table table1 table-striped">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Descripción</th>
                            <th>Cantidad</th>
                            <th>Precio unitario</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $pedido->detalles as $detalle )
                            @php
                                $tipo = $detalle->comida_id != 1 ? $detalle->comida : $detalle->combos;
                            @endphp
                            <tr>
                                <td>{{$tipo->nombre}}</td>
                                <td>{{$tipo->descripcion}}</td>
                                <td>{{$detalle->cantidad}}</td>
                                <td>{{$tipo->costo}}</td>
                                <td>{{$detalle->cantidad * $tipo->costo}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </form>
    @endsection
</div>
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.table1').DataTable();
  });

  function deleteRow(e, btn){
    if( confirm('¿Seguro de que desea realizar esta accion?') ){
        url = btn.getAttribute('url');
        $.post(url, {"_method": "DELETE", "_token": "{{ csrf_token() }}"}, function(response){
            alert(response.message);
            location.reload();
        });
      }
  }

</script>

@endsection