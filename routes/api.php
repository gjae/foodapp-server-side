<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('auth', 'HomeController@apiLogin');
Route::post('register', 'HomeController@register');
Route::group(['middleware' => 'auth:api'], function(){
    
    Route::resource('pedidos', 'FacturaController');
    Route::get('comidas', 'ComidaController@index');
    Route::get('pedidos', 'FacturaController@index');
    Route::post('pedidos/cancelar', 'FacturaController@cancelar');
    Route::get('categorias', 'CategoriaController@index');
    Route::get('comida/promos', 'ComidaController@promos');
});