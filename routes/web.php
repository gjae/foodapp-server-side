<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to( url('login') );
});

Route::get('/logout', function(){
    \Auth::logout();
});

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth'] ], function(){
    Route::get('home', 'HomeController@index')->name('home');
    Route::resource('caja', 'CajaController');
    Route::get('reportes/caja', 'CajaController@resumen')->name('resumen_de_caja');
    Route::get('reportes/tipo', 'CajaController@tipoDeReporte')->name('tipos_reportes_caja');
    Route::get('caja/reportes/dia/{dia_id}', 'CajaController@reporteDia');
    Route::resource('/comidas', 'ComidaController', [
        'names' => [
            'index' => 'comidas'
        ]
    ]);
    Route::resource('categorias', 'CategoriaController', [
        'names' => [
            'index' => 'categorias'
        ]
    ]);
    Route::resource('/combos', 'ComboComidaController', [
        'names' => [
            'index' => 'combos'
        ]
    ]);
    Route::resource('facturacion', 'FacturaController', [
        'names' => [
            'index' => 'pedidos',
            'create' => 'pedidos.create',
            'edit' => 'pedidos.edit',
            'update' => 'pedidos.update',
            'destroy' => 'pedidos.destroy'
        ]
    ]);
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
